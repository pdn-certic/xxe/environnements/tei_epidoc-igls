Un environnement pour l’édition des documents épigraphiques en EpiDoc
================

Le présent dépôt donne accès à un environnement d’annotation dédié à l’édition de corpus épigraphiques en XML-TEI selon le standard d’[EpiDoc](https://sourceforge.net/p/epidoc/wiki/Home/) (version 9.6). Il a été développé dans le cadre du programme de recherche international des [Inscriptions grecques et latines de la Syrie](https://igls.mom.fr) (IGLS).

## Descriptif de l’environnement
L’environnement fonctionne sous [XMLmind XML Editor](http://www.xmlmind.com/xmleditor/). L’extension des fonctionnalités de l’application nécessite un ensemble d’utilitaires java spécifique, l’addon pdn_tools (© PDN – Certic). Cet addon est compatible avec la version 9.5.1 Professional Edition du logiciel.

Fonctionnalités d’édition et d’annotation :
- Fichier EpiDoc vierge
- Interface d’aide à la saisie conforme au schéma d’EpiDoc (version 9.6)
- Import de transcriptions pré-encodées via l’éditeur en ligne de [Papyri.info](https://www.papyri.info/)
- Interface d’aide à l’annotation pour le balisage de la description, de l’illustration, de la bibliographie, de l’édition, de l’apparat critique, de la traduction et du commentaire historique des documents épigraphiques : langues, enrichissements typographiques, aspects formels des inscriptions, annotations diacritiques de la transcription, noms de personnes, noms de lieux, vocabulaire
- Fonctionnalités d’annotation complémentaires via l’interrogation de référentiels externes : thésaurus multilingues gérés au moyen d’[Opentheso](https://github.com/miledrousset/opentheso), collections de références bibliographiques publiées sur [Zotero](https://www.zotero.org), tables fournissant des listes de vocabulaire contrôlé propres au projet (onomastique, proposographie, sources littéraires, etc.)

## Les IGLS
Le programme des [Inscriptions grecques et latines de la Syrie](https://igls.mom.fr) (IGLS) est consacré à l’édition du corpus des inscriptions grecques et latines de la Syrie antique, c’est-à-dire du Proche-Orient moderne, dans le cadre des territoires actuels de la République arabe syrienne, de la République libanaise et du Royaume hachémite de Jordanie. Piloté à Lyon par le [laboratoire Hisoma](https://www.hisoma.mom.fr/) (Histoire et sources des mondes antiques) et publié à Beyrouth par l’[Institut français du Proche-Orient](https://www.ifporient.org/) (Ifpo) dans la [Bibliothèque archéologique et historique](https://www.persee.fr/collection/bah) (BAH), il se fonde sur l’exploitation d’archives conservées à la [Maison de l’Orient et de la Méditerranée](https://www.mom.fr/) (MOM), fédération de recherche hébergée par l’[Université Lumière Lyon 2](https://www.univ-lyon2.fr/), et sur des missions de terrain réalisées en Jordanie, au Liban et en Syrie, en collaboration étroite avec les directions des Antiquités de ces trois pays.

## Projet éditorial
Depuis 2017, le programme des IGLS est engagé dans la production d’un corpus conforme aux standards de la Text Encoding Initiative ([TEI](https://tei-c.org/)) et du modèle d’[EpiDoc](https://sourceforge.net/p/epidoc/wiki/Home/), grâce à la collaboration entre le laboratoire Hisoma, le Pôle Système d’information et réseaux (PSIR) de la Maison de l’Orient et de la Méditerranée et l’équipe du programme [Métopes](https://www.metopes.fr) (Méthodes et outils pour l’édition structurée), développé au sein du [Pôle Document numérique](https://www.unicaen.fr/recherche/mrsh/document_numerique) (PDN) de la Maison de la recherche en sciences humaines de Caen (MRSH). L’année 2018 a vu naître un protocole d’édition imprimée et numérique adapté aux contraintes techniques et à la richesse philologique des recueils d’inscriptions antiques. Ce protocole, mis en œuvre au moyen de l’éditeur [XMLmind XML Editor](http://www.xmlmind.com/xmleditor/), est adossé au thésaurus [Epigraphie.mom.fr](https://epigraphie.mom.fr) créé sur le logiciel Opentheso gestionnaire de thésaurus multilingue en ligne développé au PSIR, à une [bibliographie du groupe IGLS](https://www.zotero.org/groups/1843959/igls) publiée sur Zotero et à une collection d’images déposées sur la plate-forme [Nakala](https://www.nakala.fr/) de l’infrastructure de recherche [Huma-Num](https://www.huma-num.fr/). Le projet doit se concrétiser à la fois par une impression classique aux Presses de l’Ifpo et par la publication d’une version numérique augmentée des tomes des IGLS en libre accès.

## Contributeurs
- Julien Aliquot (Hisoma, CNRS)
- Elysabeth Hue-Gay (Hisoma, Lyon 2)
- Edith Cannet (IR Métopes, Pôle Document numérique, MRSH, CNRS)
