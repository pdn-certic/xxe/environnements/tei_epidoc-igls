<?xml version="1.0" encoding="UTF-8"?>

<!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA
    Fichier revu le 26-03-2024, Elysabeth Hue-Gay -->

<configuration xsi:schemaLocation="http://www.xmlmind.com/xmleditor/schema/configuration ../configuration/xsd/configuration.xsd"
               xmlns="http://www.xmlmind.com/xmleditor/schema/configuration"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:cfg="http://www.xmlmind.com/xmleditor/schema/configuration">


<!-- Cette commande est associée au bouton du menu pour baliser les langues dans la suite du travail. Elle est associée à un fichier langues.txt qui liste les différentes langues en leur associant leur code. -->
<command name="foreign">
	<macro repeatable="true" undoable="true">
		<sequence>
			<choice>
				<command name="selectNode" parameter="self[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
				<command name="wrap" parameter="[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
			</choice>
			<command name="pick" parameter="'Langue' false @ '%C%Slangues.txt' UTF-8"/>
        <set variable="click" expression="%_" plainString="true"/>
        <get expression="substring-after($click,' : ')"/>
       <command name="putAttribute" parameter="[implicitElement] xml:lang %_"/>
		</sequence>
	</macro>
</command>	

<!-- Cette commande permet de transformer les balises <hi rend="italic"> en <foreign xml:lang="la"> lorsqu'il convient. 
Tout le fichier est parcouru. -->
<command name="foreignLa">
    <macro>
      <sequence>
       <command name="confirm" parameter="Baliser le latin ?"/>
       <choice>
       	<command name="selectNode" parameter="[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
       	<command name="convert" parameter="[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
       </choice>
       <command name="removeAttribute"  parameter="[implicitElement] style"/>
       <command name="removeAttribute"  parameter="[implicitElement] rend"/>
       <command name="putAttribute"  parameter="[implicitElement] xml:lang la"/>
       <command name="confirm" parameter="Continuer ?"/>
       <command name="foreignLa2"/>
   </sequence>
</macro>
</command>

<command name="foreignLa2">
	<macro  repeatable="true" undoable="true">
		<sequence>
			<command name="xpathSearch" parameter="[implicitElement] (following::ns:hi[@rend='italic'])"/>
			<command name="pick" parameter="'Baliser du latin ?' false 'latin' 'autre'"/>
			<set variable="skip" expression="'%_'"/>
			<get expression="'%N'"/>
			<choice>
				<sequence>
					<test expression="$skip='latin'"/>
					<choice>
						<command name="selectNode" parameter="[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
						<command name="convert" parameter="[implicitElement] {http://www.tei-c.org/ns/1.0}foreign" />
					</choice>
					<command name="removeAttribute"  parameter="[implicitElement] style"/>	
				<command name="removeAttribute"  parameter="[implicitElement] rend"/>
				<command name="putAttribute"  parameter="[implicitElement] xml:lang la"/>
			</sequence>
			<sequence>
				<fail>
					<test expression="$skip='latin'"/>
				</fail>
			</sequence>
		</choice>
		<command name="foreignLa2"/>
	</sequence>
</macro>	
</command>

</configuration>