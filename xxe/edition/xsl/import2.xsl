<?xml version="1.0" encoding="utf-8"?>

<!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA
    Fichier revu le 19-01-2024, Elysabeth Hue-Gay -->

<xsl:stylesheet exclude-result-prefixes="office style text table draw fo xlink dc    functx   meta number tei svg chart dr3d math form       script ooo ooow oooc dom xforms xs xsd xsi" office:version="1.0" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:functx="http://www.functx.com">
    <xsl:output method="xml" encoding="utf-8" indent="no"/>
    <xsl:param name="directory"/>
    <xsl:variable name="papyriFile">
        <xsl:value-of select="concat($directory, '/export_depuis_Papyri_info/', //tei:publicationStmt/tei:idno, '_SoSOL.xml')"/>
    </xsl:variable>
    
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="tei:div[@type = 'edition' and not(@rend = 'forPrint')]">
        <xsl:for-each select="matches(., '\n')">
            <xsl:value-of select="replace('\n', '\n', 'hep')"/>
        </xsl:for-each>
        <xsl:apply-templates></xsl:apply-templates>
    </xsl:template>
    
</xsl:stylesheet>
