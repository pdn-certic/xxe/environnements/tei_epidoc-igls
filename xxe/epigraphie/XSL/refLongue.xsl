<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes=" tei svg chart dr3d math meta number office form   dc  draw fo  script ooo ooow oooc dom xforms xs xsd xsi xi style table text xlink " office:version="1.0" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:functx="http://www.functx.com">
  <xsl:output encoding="UTF-8" indent="no" method="html"/>

  <xsl:function name="functx:trim" as="xs:string" xmlns:functx="http://www.functx.com">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:sequence select="replace(replace($arg, '\s+$', ''), '^\s+', '')"/>
  </xsl:function>
  <xsl:function name="functx:escape-for-regex" as="xs:string" xmlns:functx="http://www.functx.com">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:sequence select="replace($arg, '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))', '\\$1')"/>
  </xsl:function>
  <xsl:function name="functx:substring-before-last" as="xs:string" xmlns:functx="http://www.functx.com">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:param name="delim" as="xs:string"/>
    <xsl:sequence select="
        if (matches($arg, functx:escape-for-regex($delim))) then
          replace($arg, concat('^(.*)', functx:escape-for-regex($delim), '.*'), '$1')
        else
          ''"/>
  </xsl:function>
  <xsl:function name="functx:substring-after-match" as="xs:string?" xmlns:functx="http://www.functx.com">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:param name="regex" as="xs:string"/>
    <xsl:sequence select="replace($arg, concat('^.*?', $regex), '')"/>
  </xsl:function>
  <!-- Par défaut on réalise une copie des tous les éléments dans l'arbre d'entrée dans l'arbre de sortie -->
  <xsl:template match="//tei:TEI">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head><meta charset="UTF-8"/><h1>Bibliographie de lʼinscription (références Zotero citées)</h1></head>
      <body>
        <div>
          <xsl:for-each select="//tei:bibl/tei:ref">
            <xsl:sort select="substring-after(@cRef, 'bibl:')" order="ascending"/>
            <xsl:variable name="cRefEpiDoc">
              <xsl:value-of select="substring-after(./@cRef, 'bibl:')"/>
            </xsl:variable>
            <xsl:if test="$cRefEpiDoc = document('../bibliographie.xml')//tei:body//descendant::tei:biblStruct/substring-after(@sameAs, 'items/')">
              <xsl:if test="not(preceding::tei:bibl/tei:ref[substring-after(./@cRef, 'bibl:')=$cRefEpiDoc])">
              <p>
                <span><xsl:value-of select="substring-after(@cRef, 'bibl:')"/></span><xsl:text>&#xA0;: </xsl:text>
                <xsl:apply-templates select="document('../bibliographie.xml')//tei:body//descendant::tei:biblStruct[substring-after(@sameAs, 'items/') = $cRefEpiDoc]"/>
              </p>
              </xsl:if></xsl:if>
          </xsl:for-each>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="tei:listBibl">
    <xsl:for-each select="tei:biblStruct">
      <xsl:sort select="descendant::tei:surname[1]" order="ascending"/>
      <xsl:sort select="descendant::tei:date" order="ascending"/>
      <xsl:sort select="descendant::tei:title" order="ascending"/>
      <p class="bibref">
        <xsl:apply-templates select="."/>
        <xsl:text>.&#10;</xsl:text>
      </p>
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="tei:author/tei:surname | tei:editor/tei:surname">
    <span class="nom">
      <xsl:apply-templates/>
    </span>
  </xsl:template>

  <xsl:template match="tei:author" name="author">
    <author>
      <xsl:choose>
        <xsl:when test="./tei:surname">
          <xsl:apply-templates select="./tei:surname"/>
          <xsl:text>&#xA0;</xsl:text>
          <xsl:for-each select="./tei:forename">
            <xsl:call-template name="forename"/>
          </xsl:for-each>
          <xsl:if test="./tei:nameLink">
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="./tei:nameLink"/>
          </xsl:if>
        </xsl:when>
        <xsl:when test="./tei:orgName">
          <xsl:apply-templates select="./tei:orgName"/>
        </xsl:when>
      </xsl:choose>
    </author>
  </xsl:template>
  <xsl:template match="tei:editor" name="editor">
    <editor role="{@role}">
      <xsl:choose>
        <xsl:when test="./tei:surname">

          <xsl:apply-templates select="./tei:surname"/>
          <xsl:text>&#xA0;</xsl:text>
          <xsl:for-each select="./tei:forename">
            <xsl:call-template name="forename"/>
          </xsl:for-each>
          <xsl:if test="./tei:nameLink">
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="./tei:nameLink"/>
          </xsl:if>
        </xsl:when>
        <xsl:when test="./tei:orgName">
          <xsl:apply-templates select="./tei:orgName"/>
        </xsl:when>
      </xsl:choose>
    </editor>
  </xsl:template>
  <xsl:template match="tei:edition">
    <edition>
      <xsl:apply-templates/>
      <hi rend="sup" style="typo_Exposant" aid:cstyle="typo_Exposant">e</hi>
      <xsl:text>&#xA0;éd.</xsl:text>
    </edition>
  </xsl:template>
  <!--<xsl:template match="tei:collaborator" name="collaborator">
        <collaborator>
            <xsl:for-each select="./tei:forename">
                <xsl:call-template name="forename"/>
            </xsl:for-each>
            <xsl:text>&#xA0;</xsl:text>
            <xsl:copy-of select="./tei:surname"/>
        </collaborator>
    </xsl:template> -->
  <!--   <xsl:template match="tei:imprint" name="imprint">
        <xsl:apply-templates select="./tei:pubPlace"/>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:publisher"/>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:date"/>
    </xsl:template> -->
  <xsl:template match="tei:series" name="series">
    <series>
      <xsl:apply-templates select="./tei:title[@level = 's']"/>
      <xsl:if test="./tei:biblScope">
        <xsl:text>&#xA0;</xsl:text>
        <xsl:apply-templates select="./tei:biblScope"/>
      </xsl:if>
    </series>
  </xsl:template>
  <xsl:template match="tei:extent" name="extent">
    <extent>
      <xsl:value-of select="."/>
    </extent>
  </xsl:template>
  <xsl:template match="tei:biblScope[@unit = 'page']">
    <biblScope>
      <xsl:text>p.&#xA0;</xsl:text>
      <xsl:apply-templates/>
    </biblScope>
  </xsl:template>
  <xsl:template match="tei:biblScope[@unit = 'column']">
    <biblScope>
      <xsl:text>col.&#xA0;</xsl:text>
      <xsl:apply-templates/>
    </biblScope>
  </xsl:template>
  <xsl:template match="tei:ref">
    <xsl:text>&lt;&#8201;</xsl:text>
    <ref type="{@type}" target="{@target}">
      <xsl:apply-templates/>
    </ref>
    <xsl:text>&#8201;&gt;</xsl:text>
  </xsl:template>
  <xsl:template match="tei:idno[@type = 'DOI']">
    <xsl:text>DOI&#xA0;: </xsl:text>
    <idno type="{@type}">
      <xsl:apply-templates/>
    </idno>
  </xsl:template>
  <xsl:template match="tei:forename" name="forename">
    <forename>
      <xsl:variable name="etape">
        <xsl:for-each select="tokenize(., ' ')">
          <xsl:choose>
            <xsl:when test="not(contains(., '-'))">
              <xsl:value-of select="concat(substring(., 1, 1), '.')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(substring(., 1, 1), '.&#x2011;', substring(substring-after(., '-'), 1, 1), '.')"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:text>&#xA0;</xsl:text>
        </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="functx:substring-before-last($etape, '&#xA0;')"/>
    </forename>
  </xsl:template>
  <xsl:template match="tei:title[@level = 'm']" name="title_monogr">
    <span class="oeuvre">
      
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  <xsl:template match="tei:title[@level = 'u']">
    <title>
      <xsl:choose>
        <xsl:when test="ancestor::tei:biblStruct//tei:imprint/tei:publisher">
          <xsl:attribute name="rend">italic</xsl:attribute>
          <xsl:attribute name="style">typo_Italique</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_Italique</xsl:attribute>
          <xsl:apply-templates/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </title>
  </xsl:template>
  <xsl:template match="tei:title[@level = 'j']" name="title_journal">
    <span class="oeuvre">
      <xsl:attribute name="rend">italic</xsl:attribute>
      <xsl:attribute name="style">typo_Italique</xsl:attribute>
      <xsl:attribute name="aid:cstyle">typo_Italique</xsl:attribute>
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  <xsl:template match="tei:title[@level = 'j' and @type = 'short']" name="shortTitle_journal">
    <span class="oeuvre">
      <xsl:attribute name="rend">italic</xsl:attribute>
      <xsl:attribute name="style">typo_Italique</xsl:attribute>
      <xsl:attribute name="aid:cstyle">typo_Italique</xsl:attribute>
      <xsl:apply-templates/>
    </span>
  </xsl:template>
  <xsl:template match="tei:hi[@rend = 'italic']">
    <xsl:choose>
      <xsl:when test="ancestor::tei:title[@level = 'm'] | ancestor::tei:title[@level = 'u']">
        <hi>
          <xsl:attribute name="rend">regular</xsl:attribute>
          <xsl:attribute name="style">typo_Regular</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_Regular</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:when>
      <xsl:otherwise>
        <hi>
          <xsl:attribute name="rend">italic</xsl:attribute>
          <xsl:attribute name="style">typo_Italique</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_Italique</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:hi[@rend = 'small-caps']">
    <xsl:choose>
      <xsl:when test="ancestor::*[@rend = 'italic'] | ancestor::tei:title[@level = 'm'] | ancestor::tei:title[@level = 'u'] | ancestor::tei:title[@level = 'j']">
        <hi>
          <xsl:attribute name="rend">small-caps italic</xsl:attribute>
          <xsl:attribute name="style">typo_SC_italic</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_SC_italic</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:when>
      <xsl:otherwise>
        <hi>
          <xsl:attribute name="rend">small-caps</xsl:attribute>
          <xsl:attribute name="style">typo_SC</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_SC</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:hi[@rend = 'sup']">
    <xsl:choose>
      <xsl:when test="ancestor::*[@rend = 'italic'] | ancestor::tei:title[@level = 'm'] | ancestor::tei:title[@level = 'u'] | ancestor::tei:title[@level = 'j']">
        <hi>
          <xsl:attribute name="rend">sup italic</xsl:attribute>
          <xsl:attribute name="style">typo_Exposant_Italique</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_Exposant_Italique</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:when>
      <xsl:otherwise>
        <hi>
          <xsl:attribute name="rend">sup</xsl:attribute>
          <xsl:attribute name="style">typo_Exposant</xsl:attribute>
          <xsl:attribute name="aid:cstyle">typo_Exposant</xsl:attribute>
          <xsl:apply-templates/>
        </hi>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'book']">
    <bibl sameAs="{@sameAs}" type="{@type}" xml:id="{@xml:id}">
      <xsl:if test="./tei:monogr/tei:author">
        <xsl:for-each select="./tei:monogr/tei:author">
          <!-- Pour générer "et" avant le dernier auteur-->
          <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
          <xsl:call-template name="author"/><xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(./tei:monogr/tei:author) and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:for-each select="./tei:monogr/tei:editor">
          <xsl:choose>
            <xsl:when test="not(following-sibling::tei:editor)">
              <xsl:call-template name="editor"/>
              <xsl:text>&#xA0;(éd.), </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="editor"/>
              <xsl:text>, </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:if>
      <!-- Pour afficher les collaborateurs et générer "et" avant le dernier collaborateur -->
      <!--  <xsl:if test="./tei:monogr/tei:editor[@role = 'contributor']">
                <xsl:for-each select="./tei:monogr/tei:editor[@role = 'contributor']">
                        <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:editor[@role = 'contributor'] and not(following-sibling::tei:editor[@role = 'contributor'])">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:editor[@role = 'contributor']">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>avec la collab. de </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:otherwise>
                    </xsl:choose> 
                </xsl:for-each>
                <xsl:text>, </xsl:text>
            </xsl:if>-->
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:ref"><a target="blank" href="{./tei:monogr/tei:ref}"><xsl:apply-templates select="./tei:monogr/tei:title[@level = 'm']"/></a>
        </xsl:when><xsl:otherwise><xsl:apply-templates select="./tei:monogr/tei:title[@level = 'm']"/></xsl:otherwise>
      </xsl:choose>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'volume']">

        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'volume']"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:author and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:text>éd.&#xA0;</xsl:text>
        <xsl:for-each select="./tei:monogr/tei:editor">
          <editor role="{@role}">
            <xsl:choose>
              <xsl:when test="./tei:surname">
                <xsl:for-each select="./tei:forename">
                  <xsl:call-template name="forename"/>
                </xsl:for-each>
                <xsl:if test="./tei:nameLink">
                  <xsl:text> </xsl:text>
                  <xsl:apply-templates select="./tei:nameLink"/>
                </xsl:if>
                <xsl:text>&#xA0;</xsl:text>
                <xsl:apply-templates select="./tei:surname"/>
              </xsl:when>
              <xsl:when test="./tei:orgName">
                <xsl:apply-templates select="./tei:orgName"/>
              </xsl:when>
            </xsl:choose>
          </editor>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:edition">
        <xsl:apply-templates select="./tei:monogr/tei:edition"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:pubPlace"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:publisher"/>
      <xsl:if test="./tei:series">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:series"/>
      </xsl:if>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:extent">
          <xsl:text>, </xsl:text>
          <xsl:apply-templates select="./tei:monogr/tei:extent"/>
          <!-- <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                        <xsl:text>.</xsl:text>
                    </xsl:if> -->
        </xsl:when>
        <xsl:otherwise>
          <!--  <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                    </xsl:if> -->
          <xsl:text>.</xsl:text>
        </xsl:otherwise>
      </xsl:choose><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'article']">
    <bibl sameAs="{@sameAs}" type="{@type}" xml:id="{@xml:id}">
      <xsl:if test="./tei:analytic/tei:author">
        <xsl:for-each select="./tei:analytic/tei:author">
          <!-- Pour générer "et" avant le dernier auteur-->
          <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
          <xsl:call-template name="author"/>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(./teianalytic/tei:author) and ./tei:analytic/tei:editor[@role = 'editor']">
        <xsl:for-each select="./tei:analytic/tei:editor">
          <xsl:choose>
            <xsl:when test="not(following-sibling::tei:editor)">
              <xsl:call-template name="editor"/>
              <xsl:text>&#xA0;(éd.), </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="editor"/>
              <xsl:text>, </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:if>
      <xsl:text>&#171;&#8201;</xsl:text>
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:ref">
          <a target="blank" href="{./tei:analytic/tei:ref}">
            <xsl:apply-templates select="./tei:analytic/tei:title[@level = 'a']"/></a>
        </xsl:when><xsl:otherwise><xsl:apply-templates select="./tei:analytic/tei:title[@level = 'a']"/></xsl:otherwise>
      </xsl:choose>

      <xsl:text>&#8201;&#187;</xsl:text>
      <xsl:text>, </xsl:text>
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:title[@level = 'j' and @type = 'short']">
          <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'j' and @type = 'short']"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'j']"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'volume']">
        <xsl:text>&#xA0;</xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'volume']"/>
        <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'issue']">
          <xsl:text>, </xsl:text>
          <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'issue']"/>
        </xsl:if>
      </xsl:if>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'issue'] and not(./tei:monogr/tei:biblScope[@unit = 'volume'])">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'issue']"/>
      </xsl:if>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'page']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'page']"/>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'column']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'column']"/>
      </xsl:if>
      <!--  
            <xsl:if test="./tei:analytic/tei:idno[@type = 'DOI']">
                <xsl:text>. </xsl:text>
                <xsl:apply-templates select="./tei:analytic/tei:idno[@type = 'DOI']"/>
            </xsl:if>
            <xsl:if test="./tei:analytic/tei:ref">
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="./tei:analytic/tei:ref"/>
            </xsl:if>-->
      <xsl:text>.</xsl:text><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'bookSection']">
    <bibl sameAs="{@sameAs}" type="{@type}" xml:id="{@xml:id}">
      <xsl:if test="./tei:analytic/tei:author">
        <xsl:for-each select="./tei:analytic/tei:author">
          <!-- Pour générer "et" avant le dernier auteur-->
          <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
          <xsl:call-template name="author"/>
          <xsl:text>, </xsl:text>
        </xsl:for-each>

      </xsl:if>
      <xsl:text>&#171;&#8201;</xsl:text>
      <xsl:apply-templates select="./tei:analytic/tei:title[@level = 'a']"/>
      <xsl:text>&#8201;&#187;</xsl:text>
      <xsl:text>, dans </xsl:text>
      <xsl:if test="./tei:monogr/tei:author">
        <xsl:variable name="auteur_ouvrage">
          <xsl:apply-templates select="./tei:monogr/tei:author"/>
        </xsl:variable>
        <xsl:variable name="auteur_article">
          <xsl:apply-templates select="./tei:analytic/tei:author"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$auteur_ouvrage = $auteur_article">
            <xsl:text>id., </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="./tei:monogr/tei:author">


              <!-- Pour générer "et" avant le dernier auteur-->
              <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
              <xsl:choose>
                <xsl:when test="./tei:surname">
                  <xsl:for-each select="./tei:forename">
                    <xsl:call-template name="forename"/>
                  </xsl:for-each>
                  <xsl:if test="./tei:nameLink">
                    <xsl:text> </xsl:text>
                    <xsl:apply-templates select="./tei:nameLink"/>
                  </xsl:if>
                  <xsl:text>&#xA0;</xsl:text>
                  <xsl:apply-templates select="./tei:surname"/>
                </xsl:when>
                <xsl:when test="./tei:orgName">
                  <xsl:apply-templates select="./tei:orgName"/>
                </xsl:when>
              </xsl:choose>
              <xsl:text>, </xsl:text>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:if test="not(./tei:monogr/tei:author) and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:variable name="editeur_ouvrage">
          <xsl:apply-templates select="./tei:monogr/tei:author"/>
        </xsl:variable>
        <xsl:variable name="editeur_article">
          <xsl:apply-templates select="./tei:analytic/tei:author"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$editeur_ouvrage = $editeur_article">
            <xsl:text>id.&#xA0;(éd.), </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="./tei:monogr/tei:editor">
              <xsl:choose>
                <xsl:when test="not(following-sibling::tei:editor)">
                  <xsl:choose>
                    <xsl:when test="./tei:surname">
                      <xsl:for-each select="./tei:forename">
                        <xsl:call-template name="forename"/>
                      </xsl:for-each>
                      <xsl:if test="./tei:nameLink">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:nameLink"/>
                      </xsl:if>
                      <xsl:text>&#xA0;</xsl:text>
                      <xsl:apply-templates select="./tei:surname"/>
                    </xsl:when>
                    <xsl:when test="./tei:orgName">
                      <xsl:apply-templates select="./tei:orgName"/>
                    </xsl:when>
                  </xsl:choose>
                  <xsl:text>&#xA0;(éd.), </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="./tei:surname">
                      <xsl:for-each select="./tei:forename">
                        <xsl:call-template name="forename"/>
                      </xsl:for-each>
                      <xsl:if test="./tei:nameLink">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:nameLink"/>
                      </xsl:if>
                      <xsl:text>&#xA0;</xsl:text>
                      <xsl:apply-templates select="./tei:surname"/>
                    </xsl:when>
                    <xsl:when test="./tei:orgName">
                      <xsl:apply-templates select="./tei:orgName"/>
                    </xsl:when>
                  </xsl:choose>
                  <xsl:text>, </xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <!--     <xsl:if test="./tei:monogr/tei:editor[@role = 'contributor']">
                <xsl:for-each select="./tei:monogr/tei:editor[@role = 'contributor']">
                    <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:editor[@role = 'contributor'] and not(following-sibling::tei:editor[@role = 'contributor'])">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:editor[@role = 'contributor']">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>avec la collab. de </xsl:text>
                            <xsl:call-template name="editor"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>, </xsl:text>
            </xsl:if> -->
      <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'm']"/>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'volume']">

        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'volume']"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:author and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:text>éd.&#xA0;</xsl:text>
        <xsl:for-each select="./tei:monogr/tei:editor">
          <editor role="{@role}">
            <xsl:choose>
              <xsl:when test="./tei:surname">
                <xsl:for-each select="./tei:forename">
                  <xsl:call-template name="forename"/>
                </xsl:for-each>
                <xsl:if test="./tei:nameLink">
                  <xsl:text> </xsl:text>
                  <xsl:apply-templates select="./tei:nameLink"/>
                </xsl:if>
                <xsl:text>&#xA0;</xsl:text>
                <xsl:apply-templates select="./tei:surname"/>
              </xsl:when>
              <xsl:when test="./tei:orgName">
                <xsl:apply-templates select="./tei:orgName"/>
              </xsl:when>
            </xsl:choose>
          </editor>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:edition">
        <xsl:apply-templates select="./tei:monogr/tei:edition"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:pubPlace"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:publisher"/>
      <xsl:if test="./tei:series">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:series"/>
      </xsl:if>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'page']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'page']"/>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'column']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'column']"/>
      </xsl:if>
      <!--     <xsl:if test="./tei:analytic/tei:ref">
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="./tei:analytic/tei:ref"/>
            </xsl:if>-->
      <xsl:text>.</xsl:text><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'dictionaryArticle']">
    <bibl sameAs="{@sameAs}" type="{@type}" xml:id="{@xml:id}">
      <xsl:if test="./tei:analytic/tei:author">
        <xsl:for-each select="./tei:analytic/tei:author">
          <!-- Pour générer "et" avant le dernier auteur-->
          <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
          <xsl:call-template name="author"/>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="not(./tei:analytic/tei:author) and ./tei:analytic/tei:editor[@role = 'editor']">

        <xsl:for-each select="./tei:analytic/tei:editor">
          <xsl:choose>
            <xsl:when test="not(following-sibling::tei:editor)">
              <xsl:call-template name="editor"/>
              <xsl:text>&#xA0;(éd.), </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="editor"/>
              <xsl:text>, </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:if>
      <xsl:text>&#171;&#8201;</xsl:text>
      <xsl:apply-templates select="./tei:analytic/tei:title[@level = 'a']"/>
      <xsl:text>&#8201;&#187;</xsl:text>
      <xsl:text>, dans </xsl:text>
      <xsl:if test="./tei:monogr/tei:author">
        <xsl:variable name="auteur_ouvrage">
          <xsl:apply-templates select="./tei:monogr/tei:author"/>
        </xsl:variable>
        <xsl:variable name="auteur_article">
          <xsl:apply-templates select="./tei:analytic/tei:author"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$auteur_ouvrage = $auteur_article">
            <xsl:text>id., </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="./tei:monogr/tei:author">


              <!-- Pour générer "et" avant le dernier auteur-->
              <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
              <xsl:choose>
                <xsl:when test="./tei:surname">
                  <xsl:for-each select="./tei:forename">
                    <xsl:call-template name="forename"/>
                  </xsl:for-each>
                  <xsl:if test="./tei:nameLink">
                    <xsl:text> </xsl:text>
                    <xsl:apply-templates select="./tei:nameLink"/>
                  </xsl:if>
                  <xsl:text>&#xA0;</xsl:text>
                  <xsl:apply-templates select="./tei:surname"/>
                </xsl:when>
                <xsl:when test="./tei:orgName">
                  <xsl:apply-templates select="./tei:orgName"/>
                </xsl:when>
              </xsl:choose>
              <xsl:text>, </xsl:text>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:if test="not(./tei:monogr/tei:author) and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:variable name="editeur_ouvrage">
          <xsl:apply-templates select="./tei:monogr/tei:author"/>
        </xsl:variable>
        <xsl:variable name="editeur_article">
          <xsl:apply-templates select="./tei:analytic/tei:author"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$editeur_ouvrage = $editeur_article">
            <xsl:text>id.&#xA0;(éd.), </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="./tei:monogr/tei:editor">
              <xsl:choose>
                <xsl:when test="not(following-sibling::tei:editor)">
                  <xsl:choose>
                    <xsl:when test="./tei:surname">
                      <xsl:for-each select="./tei:forename">
                        <xsl:call-template name="forename"/>
                      </xsl:for-each>
                      <xsl:if test="./tei:nameLink">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:nameLink"/>
                      </xsl:if>
                      <xsl:text>&#xA0;</xsl:text>
                      <xsl:apply-templates select="./tei:surname"/>
                    </xsl:when>
                    <xsl:when test="./tei:orgName">
                      <xsl:apply-templates select="./tei:orgName"/>
                    </xsl:when>
                  </xsl:choose>
                  <xsl:text>&#xA0;(éd.), </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="./tei:surname">
                      <xsl:for-each select="./tei:forename">
                        <xsl:call-template name="forename"/>
                      </xsl:for-each>
                      <xsl:if test="./tei:nameLink">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:nameLink"/>
                      </xsl:if>
                      <xsl:text>&#xA0;</xsl:text>
                      <xsl:apply-templates select="./tei:surname"/>
                    </xsl:when>
                    <xsl:when test="./tei:orgName">
                      <xsl:apply-templates select="./tei:orgName"/>
                    </xsl:when>
                  </xsl:choose>
                  <xsl:text>, </xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>

      <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'm']"/>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'volume']">

        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'volume']"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:author and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:text>éd.&#xA0;</xsl:text>
        <xsl:for-each select="./tei:monogr/tei:editor">
          <editor role="{@role}">
            <xsl:choose>
              <xsl:when test="./tei:surname">
                <xsl:for-each select="./tei:forename">
                  <xsl:call-template name="forename"/>
                </xsl:for-each>
                <xsl:if test="./tei:nameLink">
                  <xsl:text> </xsl:text>
                  <xsl:apply-templates select="./tei:nameLink"/>
                </xsl:if>
                <xsl:text>&#xA0;</xsl:text>
                <xsl:apply-templates select="./tei:surname"/>
              </xsl:when>
              <xsl:when test="./tei:orgName">
                <xsl:apply-templates select="./tei:orgName"/>
              </xsl:when>
            </xsl:choose>
          </editor>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:edition">
        <xsl:apply-templates select="./tei:monogr/tei:edition"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:imprint/tei:pubPlace">
        <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:pubPlace"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:imprint/tei:publisher">
        <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:publisher"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="./tei:series">
        <xsl:apply-templates select="./tei:series"/>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      <xsl:text>, </xsl:text>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'page']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'page']"/>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'column']">
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'column']"/>
      </xsl:if>
      <!--     <xsl:if test="./tei:analytic/tei:ref">
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="./tei:analytic/tei:ref"/>
            </xsl:if>-->
      <xsl:text>.</xsl:text><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'thesis']" xml:id="{@xml:id}">
    <bibl sameAs="{@sameAs}" type="{@type}">
      <xsl:if test="./tei:monogr/tei:author">
        <xsl:for-each select="./tei:monogr/tei:author">
          <xsl:choose>
            <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
              <xsl:text> et </xsl:text>
              <xsl:call-template name="author"/>
            </xsl:when>
            <xsl:when test="preceding-sibling::tei:author">
              <xsl:text>, </xsl:text>
              <xsl:call-template name="author"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="author"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:text>, </xsl:text>
      </xsl:if>
      <!--  <xsl:for-each select="tei:collaborator">
            <xsl:choose>
                <xsl:when test="not(preceding-sibling::tei:collaborator)">
                    <xsl:text>avec la collab. de </xsl:text>
                    <xsl:call-template name="collaborator"/>
                    <xsl:text>, </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="collaborator"/>
                    <xsl:text>, </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each> -->
      <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'u']"/>
      <xsl:if test="./tei:monogr/tei:note[@type = 'memoire']">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:note[@type = 'memoire']"/>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:biblScope[@unit = 'volume']">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:biblScope[@unit = 'volume']"/>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:author and ./tei:monogr/tei:editor[@role = 'editor']">
        <xsl:text>, éd.&#xA0;</xsl:text>
        <xsl:for-each select="./tei:monogr/tei:editor">
          <xsl:call-template name="editor"/>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="./tei:monogr/tei:edition">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:edition"/>
      </xsl:if>

      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:pubPlace"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:publisher"/>
      <xsl:if test="./tei:series">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:series"/>
      </xsl:if>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:extent">
          <xsl:text>, </xsl:text>
          <xsl:apply-templates select="./tei:monogr/tei:extent"/>
          <!-- <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                        <xsl:text>.</xsl:text>
                    </xsl:if> -->
        </xsl:when>
        <xsl:otherwise>
          <!--  <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                    </xsl:if> -->
          <xsl:text>.</xsl:text>
        </xsl:otherwise>
      </xsl:choose><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
  <xsl:template match="tei:biblStruct[@type = 'manuscript']" xml:id="{@xml:id}">
    <bibl sameAs="{@sameAs}" type="{@type}">
      <xsl:if test="./tei:monogr/tei:author">
        <xsl:for-each select="./tei:monogr/tei:author">
          <!-- Pour générer "et" avant le dernier auteur-->
          <!--     <xsl:choose>
                        <xsl:when test="preceding-sibling::tei:author and not(following-sibling::tei:author)">
                            <xsl:text> et </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:when test="preceding-sibling::tei:author">
                            <xsl:text>, </xsl:text>
                            <xsl:call-template name="author"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="author"/>
                        </xsl:otherwise>
                    </xsl:choose> -->
          <xsl:call-template name="author"/>
          <xsl:text>, </xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:apply-templates select="./tei:monogr/tei:title[@level = 'u']"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:note[@type = 'place']"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="./tei:monogr/tei:note[@type = 'repository']"/>
      <xsl:if test="./tei:monogr/tei:imprint/tei:date">
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="./tei:monogr/tei:imprint/tei:date"/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="./tei:monogr/tei:extent">
          <xsl:text>, </xsl:text>
          <xsl:apply-templates select="./tei:monogr/tei:extent"/>
          <!-- <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                        <xsl:text>.</xsl:text>
                    </xsl:if> -->
        </xsl:when>
        <xsl:otherwise>
          <!--  <xsl:if test="./tei:monogr/tei:ref">
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="./tei:monogr/tei:ref"/>
                    </xsl:if> -->
          <xsl:text>.</xsl:text>
        </xsl:otherwise>
      </xsl:choose><xsl:text> (</xsl:text>
      <a target="blank" href="{@sameAs}">
        <xsl:text>↗</xsl:text>
      </a>
      <xsl:text>)</xsl:text>
    </bibl>
  </xsl:template>
</xsl:stylesheet>
