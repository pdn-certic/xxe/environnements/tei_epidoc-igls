function layoutMarginNotes() {
    var previousNote = null;
    $(".manchette").each(function() {
        if (previousNote != null && (previousNote.offset().top + previousNote.height() + 15 >= $(this).offset().top)) {
            $(this).css("top", (previousNote.offset().top + previousNote.height() + 6) + "px");
        }
        previousNote = $(this);
    });

}