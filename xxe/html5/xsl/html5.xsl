<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei" version="2.0">
	  <xsl:import href="bibliographie.xsl"/>
	<xsl:output encoding="UTF-8" indent="yes" method="html"/>
	<xsl:key name="index" match="tei:index" use="@indexName"/>
	<xsl:key name="sousindex" match="tei:index" use="@synch"/>
	<xsl:key name="term" match="tei:term" use="."/>
	<xsl:variable name="cellRendition">
		<xsl:copy-of select="//tei:tagsDecl"/>
	</xsl:variable>
	<xsl:variable name="mainLang">
		<xsl:value-of select="substring-before(//tei:language/@ident,'-')"/>
	</xsl:variable>
	<!-- ###### structure ###### -->
	<!-- ###### appel traitement des notes ###### -->
	<!-- ###### appel génération index ###### -->
	<xsl:template match="/">
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
		<html>
			<head>
				<meta charset="UTF-8"/>
				<title>
					<xsl:value-of select="//tei:titlePart[@type='main']"/>
				</title>
				<link href="ressources/tei.css" rel="stylesheet" type="text/css"/>
				<link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css"/>
				<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"/>
				<script src="ressources/tei.js" charset="utf-8" type="text/javascript"/>
				<script>
      			jQuery(document).ready(function(){
					layoutMarginNotes();
				});
      		</script>
			</head>
			<body>
				<nav id="mainnav">
					<a href="#" class="hide">
						<xsl:value-of select="document('i18n.xml')//entry[child::key[text()='hideTDM']]/text[@xml:lang=$mainLang]"/>
					</a>
					<a href="#" class="show">
						<xsl:value-of select="document('i18n.xml')//entry[child::key[text()='showTDM']]/text[@xml:lang=$mainLang]"/>
					</a>
					<ul id="nav">
						<xsl:apply-templates select="//tei:head[@subtype]" mode="toc"/>
						<xsl:if test="//tei:index">
							<li class="head1">
								<a href="{concat('#','indextitle')}">
									<xsl:text>Index</xsl:text>
								</a>
							</li>
						</xsl:if>
					</ul>
				</nav>
				<article>
					<xsl:apply-templates/>
					<div class="notes">
						<!-- CHANGEMENT MARIE POUR BIBLIO -->
						<xsl:for-each select="descendant::tei:note[@type!='memoire']">
						<!-- CHANGEMENT MARIE POUR BIBLIO <xsl:for-each select="descendant::tei:note"> 
					FIN CHANGEMENT MARIE POUR BIBLIO-->
							<xsl:call-template name="notesDiv"/>
						</xsl:for-each>
					</div>
					<xsl:if test="descendant::tei:index">
						<div class="mainIndex">
							<xsl:call-template name="generateIndex"/>
						</div>
					</xsl:if>
				</article>
				<footer>
					<xsl:text>TEI – Métopes</xsl:text>
				</footer>
				<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"/>
				<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"/>
				<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.min.js"/>
				<script type="text/javascript">
    $(function($){
        var addToAll = true;
        var gallery = true;
        var titlePosition = 'inside';
        $(addToAll ? 'img' : 'img.fancybox').each(function(){
            var $this = $(this);
            var title = $this.attr('title');
            var src = $this.attr('data-big') || $this.attr('src');
            var a = $('<a href="#" class="fancybox"/>').attr('href', src).attr('title', title);
            $this.wrap(a);
        });
        if (gallery)
            $('a.fancybox').attr('rel', 'fancyboxgallery');
        $('a.fancybox').fancybox({
            titlePosition: titlePosition
        });
    });
    $.noConflict();
			</script>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="tei:teiHeader"/>
	<!-- ###### nav tdm ###### -->
	<xsl:template match="tei:head[@subtype]" mode="toc">
		<xsl:variable name="headType">
			<xsl:choose>
				<xsl:when test="contains(@subtype,'level')">
					<xsl:value-of select="substring-after(@subtype,'level')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="parent::tei:div/@type"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<li>
			<xsl:attribute name="class">
				<xsl:value-of select="concat('head',$headType)"/>
			</xsl:attribute>
			<a href="{concat('#',generate-id())}">
				<xsl:apply-templates/>
			</a>
		</li>
	</xsl:template>
	<!-- ###### niveaux de titres ###### -->
	<xsl:template match="tei:titlePart">
		<xsl:choose>
			<xsl:when test="@type='main'">
				<h1>
					<xsl:apply-templates/>
				</h1>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:attribute name="class">
						<xsl:value-of select="@style"/>
					</xsl:attribute>
					<xsl:apply-templates/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tei:head[@subtype]">
		<xsl:choose>
			<xsl:when test="@subtype='level1'">
				<h2>
					<anchor id="{generate-id()}"/>
					<xsl:apply-templates/>
					<a href="#mainnav" class="totdm">↑</a>
				</h2>
			</xsl:when>
			<xsl:when test="@subtype='level2'">
				<h3>
					<anchor id="{generate-id()}"/>
					<xsl:apply-templates/>
				</h3>
			</xsl:when>
			<xsl:when test="@subtype='level3'">
				<h4>
					<anchor id="{generate-id()}"/>
					<xsl:apply-templates/>
				</h4>
			</xsl:when>
			<xsl:when test="@subtype='level4'">
				<h5>
					<anchor id="{generate-id()}"/>
					<xsl:apply-templates/>
				</h5>
			</xsl:when>
			<xsl:when test="@subtype='level5'">
				<h6>
					<anchor id="{generate-id()}"/>
					<xsl:apply-templates/>
				</h6>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:attribute name="class">
						<xsl:value-of select="concat('head',substring-after(@subtype,'level')"/>
					</xsl:attribute>
					<xsl:apply-templates/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ###### div/section ###### -->
	<xsl:template match="tei:div[@type]">
		<section>
			<xsl:attribute name="class">
				<xsl:value-of select="@type"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</section>
	</xsl:template>
	<xsl:template match="tei:p">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<!-- ###### citations ###### -->
	<xsl:template match="tei:quote">
		<blockquote>
			<xsl:apply-templates/>
		</blockquote>
	</xsl:template>
	<!-- ###### notes ###### -->
	<!-- CHANGEMENT MARIE POUR BIBLIO -->
	<xsl:template match="tei:note[@type!='memoire']">
			<!-- <xsl:template match="tei:note"> FIN CHANGEMENT MARIE BIBLIO-->
		<a class="tonote">
			<xsl:attribute name="href">
				<xsl:value-of select="concat('#note',@n)"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="concat('note',@n,'return')"/>
			</xsl:attribute>
			<sup>
				<xsl:value-of select="@n"/>
			</sup>
		</a>
		<span class="manchette">
			<a class="appel"><xsl:value-of select="@n"/>. </a>
			<xsl:value-of select="substring(.,1,15)"/>
			<a class="suite"><xsl:attribute name="href"><xsl:value-of select="concat('#note',@n)"/></xsl:attribute> (…) </a>
		</span>
	</xsl:template>
	<xsl:template name="notesDiv">
		<div class="note">
			<a class="noteNum">
				<xsl:attribute name="name">
					<xsl:value-of select="concat('note',@n)"/>
				</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="concat('#note',@n,'return')"/>
				</xsl:attribute>
				<xsl:value-of select="concat(@n,'. ')"/>
			</a>
			<xsl:apply-templates/>
		</div>
	</xsl:template>
	<!-- ###### données du front ###### -->
	<xsl:template match="tei:docAuthor">
		<p class="docAuthor">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<xsl:template match="tei:affiliation">
		<p class="affiliation">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<!-- ###### bibli ###### -->
	<xsl:template match="tei:bibl">
		<p class="bibl">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<!-- ###### séparateur ###### -->
	<xsl:template match="tei:lb">
		<br/>
	</xsl:template>
	<!-- ###### enrichissements typo ###### -->
	<xsl:template match="tei:hi[@rend='italic']">
		<em>
			<xsl:apply-templates/>
		</em>
	</xsl:template>
	<xsl:template match="tei:hi[@rend='bold']">
		<strong>
			<xsl:apply-templates/>
		</strong>
	</xsl:template>
	<xsl:template match="tei:hi[@rend='sup']">
		<sup>
			<xsl:apply-templates/>
		</sup>
	</xsl:template>
	<xsl:template match="tei:hi[@rend='sup italic']">
		<sup>
			<em>
				<xsl:apply-templates/>
			</em>
		</sup>
	</xsl:template>
	<xsl:template match="tei:hi[@rend='small-caps']">
		<span class="small-caps">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<!-- ###### tables ###### -->
	<xsl:template match="tei:table">
		<div class="table" anchor="@xml:id">
			<table>
				<xsl:if test="tei:head">
					<caption>
						<xsl:apply-templates select="tei:head"/>
					</caption>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="tei:row[@role='label' and not(preceding::tei:row)]">
						<thead>
							<xsl:apply-templates select="tei:row[@role='label' and not(preceding::tei:row)]"/>
						</thead>
						<tbody>
							<xsl:apply-templates select="tei:row[not(@role='label' and not(preceding::tei:row))]"/>
						</tbody>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="tei:row"/>
					</xsl:otherwise>
				</xsl:choose>
			</table>
			<!--xsl:apply-templates select="tei:note"/-->
		</div>
	</xsl:template>
	<xsl:template match="tei:row">
		<tr>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>
	<xsl:template match="tei:cell">
		<xsl:variable name="cellname">
			<xsl:choose>
				<xsl:when test="parent::tei:row[@role='label' and not(preceding::tei:row)]">th</xsl:when>
				<xsl:otherwise>td</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:element name="{$cellname}">
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="name(.)='cols'">
						<xsl:attribute name="colspan">
							<xsl:value-of select="."/>
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="name(.)='rows'">
						<xsl:attribute name="rowspan">
							<xsl:value-of select="."/>
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="name(.)='rendition'">
						<xsl:variable name="cellStyle" select="substring-after(.,'#')"/>
						<xsl:attribute name="style">
							<xsl:value-of select="$cellRendition//tei:rendition[@xml:id=$cellStyle]"/>
						</xsl:attribute>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- ###### listes ###### -->
	<xsl:template match="tei:list">
		<xsl:choose>
			<xsl:when test="@type='unordered'">
				<ul>
					<xsl:apply-templates/>
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<ol>
					<xsl:apply-templates/>
				</ol>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tei:item">
		<li>
			<xsl:apply-templates/>
		</li>
	</xsl:template>
	<!-- ###### images ###### -->
	<xsl:template match="tei:figure">
		<figure>
			<xsl:apply-templates/>
		</figure>
	</xsl:template>
	<xsl:template match="tei:head[parent::tei:figure]">
		<figcaption>
			<xsl:apply-templates/>
		</figcaption>
	</xsl:template>
	<xsl:template match="tei:graphic">
		<img class="fancybox">
			<xsl:attribute name="src">
				<xsl:value-of select="concat('../',@url)"/>
			</xsl:attribute>
			<xsl:attribute name="alt">
				<xsl:value-of select="following-sibling::tei:head"/>
			</xsl:attribute>
		</img>
	</xsl:template>
	<!-- ###### hyperliens ###### -->
	<xsl:template match="tei:ref">
		<a class="hyperlink">
			<xsl:attribute name="href">
				<xsl:value-of select="@target"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</a>
	</xsl:template>
	<!-- ###### index ###### -->
	<xsl:template name="generateIndex">
		<h2 id="indextitle">Index</h2>
		<ul>
			<xsl:for-each select="(//tei:index[@indexName])[generate-id(.)=generate-id(key('index',@indexName)[1])]">
				<xsl:sort select="@indexName" order="ascending"/>
				<li>
					<xsl:value-of select="@indexName"/>
					<xsl:variable name="index" select="@indexName"/>
					<xsl:variable name="term" select="child::tei:term"/>
					<!-- niveau 2 -->
					<ul>
						<xsl:choose>
							<xsl:when test="//tei:index[@synch]">
								<!--index = <xsl:value-of select="$index"/>|-->
								<xsl:for-each select="(//tei:index[@indexName=$index])[generate-id(.)=generate-id(key('sousindex',@synch)[1])]">
									<xsl:sort select="@synch" order="ascending"/>
									<li>
										<xsl:value-of select="@synch"/>
										<xsl:variable name="sousindex" select="@synch"/>
										<!-- niveau 3 -->
										<ul>
											<xsl:for-each select="(//tei:term[parent::tei:index[@synch=$sousindex]])[generate-id(.)=generate-id(key('term',.)[1])]">
												<xsl:sort select="." order="ascending"/>
												<li>
													<xsl:value-of select="."/>
													<br/>
													<!-- start liens -->
													<xsl:variable name="nbofocc">
														<xsl:value-of select="count(//tei:term[parent::tei:index[@indexName=$index]][text()=$term])"/>
													</xsl:variable>
													<xsl:comment>nbofocc = <xsl:value-of select="$nbofocc"/></xsl:comment>
													<xsl:for-each select="//tei:term[parent::tei:index[@synch=$sousindex]][text()=$term]">
														<a class="index">
															<xsl:attribute name="href">
																<xsl:value-of select="concat('#',generate-id(.))"/>
															</xsl:attribute>
															<xsl:number count="//tei:term[parent::tei:index[@synch=$sousindex]][text()=$term]" format="1" level="any"/>
															<xsl:text>    </xsl:text>
														</a>
														<anchor>
															<xsl:attribute name="id">
																<xsl:value-of select="concat('back',generate-id(.))"/>
															</xsl:attribute>
														</anchor>
													</xsl:for-each>
													<!-- end liens -->
												</li>
											</xsl:for-each>
										</ul>
									</li>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<!--ici-->
								<xsl:choose>
									<xsl:when test="$index!=$term">
										<xsl:for-each select="//tei:term[parent::tei:index[@indexName=$index]]">
											<a class="index">
												<xsl:attribute name="href">
													<xsl:value-of select="concat('#',generate-id(.))"/>
												</xsl:attribute>
												<xsl:number count="//tei:term[parent::tei:index[@indexName=$index]]" format="1" level="any"/>
												<xsl:text>    </xsl:text>
											</a>
											<anchor>
												<xsl:attribute name="id">
													<xsl:value-of select="concat('back',generate-id(.))"/>
												</xsl:attribute>
											</anchor>
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<xsl:for-each select="(//tei:term[parent::tei:index[@indexName=$index]])[generate-id(.)=generate-id(key('term',.)[1])]">
											<xsl:sort select="." order="ascending"/>
											<li>
												<xsl:value-of select="."/>
												<br/>
												<!-- start liens -->
												<xsl:variable name="nbofocc">
													<xsl:value-of select="count(//tei:term[parent::tei:index[@indexName=$index]][text()=$term])"/>
												</xsl:variable>
												<xsl:comment>nbofocc = <xsl:value-of select="$nbofocc"/></xsl:comment>
												<xsl:for-each select="//tei:term[parent::tei:index[@indexName=$index]][text()=$term]">
													<a class="index">
														<xsl:attribute name="href">
															<xsl:value-of select="concat('#',generate-id(.))"/>
														</xsl:attribute>
														<xsl:number count="//tei:term[parent::tei:index[@indexName=$index]][text()=$term]" format="1" level="any"/>
														<xsl:text>    </xsl:text>
													</a>
													<anchor>
														<xsl:attribute name="id">
															<xsl:value-of select="concat('back',generate-id(.))"/>
														</xsl:attribute>
													</anchor>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</ul>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>
	<xsl:template name="List">
		<xsl:param name="list"/>
		<xsl:variable name="first" select="substring-before($list, ':')"/>
		<xsl:variable name="remaining" select="substring-after($list, ':')"/>
		<xsl:choose>
			<xsl:when test="$remaining!=''">
				<li>
					<xsl:value-of select="$first"/>
					<ul>
						<li>
							<xsl:value-of select="$remaining"/>
						</li>
					</ul>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<li>
					<xsl:value-of select="$list"/>
				</li>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tei:term">
		<span class="index">
			<anchor>
				<xsl:attribute name="id">
					<xsl:value-of select="generate-id(.)"/>
				</xsl:attribute>
			</anchor>
			<a class="lienIndex"><xsl:attribute name="href"><xsl:value-of select="concat('#back',generate-id(.))"/></xsl:attribute>#</a>
		</span>
	</xsl:template>

</xsl:stylesheet>
