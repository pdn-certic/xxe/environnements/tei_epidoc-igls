<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/TR/xhtml/strict" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei xhtml">

  <!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA, PDN MRSH Caen-->
  <!-- cette xsl va chercher dans le fichier Epidoc les terms qui n'existent pas dans le fichier index_general.xml -->
  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="tei:list">
    <xsl:variable name="item">
      <xsl:value-of select="./tei:item"/>
    </xsl:variable>
    <xsl:copy-of select="./tei:item"/>
    <xsl:for-each select="document('../IGLS_8-1_0260.xml', .)//tei:teiHeader//descendant::tei:index[@indexName = 'indexGeneral']/tei:term">
      <xsl:if test="not(. = $item)"/>
      <item>
        <xsl:apply-templates/>
      </item>
    </xsl:for-each>
  </xsl:template>
  <xsl:template match="tei:note[@type = 'expression']"/>
</xsl:stylesheet>
