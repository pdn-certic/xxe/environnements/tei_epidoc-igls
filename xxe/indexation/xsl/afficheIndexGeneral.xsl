<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/TR/xhtml/strict" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei xhtml">

    <!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, mai 2018
    HiSoMA, PDN MRSH Caen-->

    <xsl:output method="text" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="/">
        <xsl:text>00-nouvelle entrée</xsl:text>
        <xsl:text>
</xsl:text>
        <xsl:for-each select="//tei:item[not(ancestor::tei:item)]">
            <xsl:sort select="
            translate(., 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzÀàÁáÂâÃãÄäÅåÆæÇçÈèÉéÊêËëÌìÍíÎîÏïÐðÑñÒòÓóÔôÕõÖöØøÙùÚúÛûÜüÝýÞþ', 'aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzzaaaaaaaaaaaaææcceeeeeeeeiiiiiiiiððnnooooooooooøøuuuuuuuuyyþþ')"/>

                    <xsl:choose>
                        <xsl:when test="child::tei:list">
                          <!--  <xsl:text>– </xsl:text> --><xsl:value-of select="node()[following-sibling::tei:list]"/><xsl:text>
</xsl:text>
                            <xsl:for-each select=".//tei:item">
                                <xsl:value-of select="parent::tei:list/preceding-sibling::node()"/>
                                <xsl:text> – </xsl:text>
                                <xsl:value-of select="."/>
                                <xsl:text>
</xsl:text>
                            </xsl:for-each>
                            
                        </xsl:when>
                        <xsl:otherwise>
                         <!--   <xsl:text>– </xsl:text> -->
                            <xsl:value-of select="."/><xsl:text>
</xsl:text>
                     
                        </xsl:otherwise>   
                        
                    </xsl:choose>
            
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
