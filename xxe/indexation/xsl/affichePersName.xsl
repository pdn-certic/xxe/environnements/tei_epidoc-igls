<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/TR/xhtml/strict" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei xhtml">

    <!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA, PDN MRSH Caen-->

    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:persName">
        <xsl:variable name="id">
            <xsl:value-of select="substring-after(./@ref, '#')"/>
        </xsl:variable>
        <seg>
            <xsl:value-of select="$id"/>
            <xsl:value-of select="document('prosopographie.xml')//descendant::tei:body//descendant::tei:person[@xml:id = $id]/child::tei:persName"/>
        <xsl:value-of select="document('../prosopographie.xml')//tei:title"></xsl:value-of></seg>
       
    </xsl:template>

</xsl:stylesheet>
