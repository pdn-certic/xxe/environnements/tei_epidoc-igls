<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/TR/xhtml/strict" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei xhtml">
   
    <!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA, PDN MRSH Caen-->
    
    <xsl:output method="text" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <xsl:for-each-group select="//tei:bibl[@type = 'ancientWork']" group-by="@xml:id">
            <xsl:sort select="tei:title" order="ascending"/>
            <xsl:if test="./tei:author">
                <xsl:value-of select="./tei:author"/>
                <xsl:text>:</xsl:text>
            </xsl:if>
            <xsl:value-of select="./tei:title"/>
            <xsl:text> [</xsl:text>
            <xsl:value-of select="./@xml:id"/>
            <xsl:text>] </xsl:text>
            <xsl:text>
</xsl:text>
        </xsl:for-each-group>
    </xsl:template>



</xsl:stylesheet>
