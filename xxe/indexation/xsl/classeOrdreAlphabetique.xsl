<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/TR/xhtml/strict" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei xhtml">

    <!--  Configuration XMLMind pour environnement TEI-Epidoc
    Projet IGLS, novembre 2017
    HiSoMA, PDN MRSH Caen-->

    <xsl:output method="xml" encoding="UTF-8" indent="no"/>
    <!--<xsl:strip-space elements="*"/> -->


    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

<!-- Classement alphabétique dans la table prosopographique -->
  <xsl:template match="tei:div[@type='visitors']/tei:listPerson">
        <listPerson>
            <xsl:for-each select="./tei:person">
              <xsl:sort select="translate(tei:persName/tei:surname, '’', ' ')" order="ascending"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listPerson>
    </xsl:template>
<xsl:template match="tei:div[@type='historicals']/tei:listPerson">
        <listPerson>
            <xsl:for-each select="./tei:person">
              <xsl:sort select="translate(tei:persName, '’', ' ')" order="ascending"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listPerson>
    </xsl:template>
    <xsl:template match="tei:div[@type='animals']/tei:listPerson">
        <listPerson>
            <xsl:for-each select="./tei:person">
              <xsl:sort select="translate(tei:persName, '’', ' ')" order="ascending"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listPerson>
    </xsl:template>


  <xsl:template match="tei:div[@type='attested']/tei:listPerson">
    <listPerson>
      <xsl:for-each select="./tei:person">  
        <xsl:sort select="translate(tei:persName, '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </listPerson>
  </xsl:template>

  <xsl:template match="tei:div[@type='divines']/tei:listPerson">
    <listPerson>
      <xsl:for-each select="./tei:person">
        <xsl:sort select="translate(tei:persName[@xml:lang='fr'], '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </listPerson>
  </xsl:template>

  <xsl:template match="tei:div[@type='sovereigns']/tei:listPerson">
    <listPerson>
      <xsl:for-each select="./tei:person">
        <xsl:sort select="translate(tei:persName[@xml:lang='fr'], '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </listPerson>
  </xsl:template>

  <xsl:template match="tei:div[@type='groups']/tei:listOrg">
    <listOrg>
      <xsl:for-each select="./tei:org">
        <xsl:sort select="translate(tei:orgName, '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </listOrg>
  </xsl:template>

<xsl:template match="tei:div[@type='relations']/tei:listRelation">
  <listRelation>
    <xsl:for-each select="./tei:relation">
        <xsl:sort select="translate(tei:desc, '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
  </listRelation>
</xsl:template>

<!-- Classement alphabétique de la table des lieu de conservation-->

<xsl:template match="tei:listOrg">
  <listOrg>
    <xsl:for-each select="./tei:org">
        <xsl:sort select="translate(tei:orgName, '’', ' ')" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
  </listOrg>
</xsl:template>

<!-- Classement alphabétique de la table des sources -->
<xsl:template match="tei:div[@type='authors']/tei:listPerson">
        <listPerson>
            <xsl:for-each select="./tei:person">
              <xsl:sort select="translate(tei:persName, '’', ' ')" order="ascending"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listPerson>
    </xsl:template>

    <xsl:template match="tei:listBibl">
        <listBibl>
            <xsl:for-each select="./tei:bibl">
              
               
                <xsl:sort select="translate(tei:*[1],'ἀἁἂἃἄἅἆἇἈἉἊἋἌἍἎἏὰάᾀᾁᾂᾃᾄᾅᾆᾇᾈᾉᾊᾋᾌᾍᾎᾏᾰᾱᾲᾳᾴᾶᾷᾸᾹᾺΆᾼΆΑάαΒβϐΓγΔδἐἑἒἓἔἕἘἙἚἛἜἝὲέῈΈΈΕέεϵ϶ΖζἠἡἢἣἤἥἦἧἨἩἪἫἬἭἮἯὴήᾐᾑᾒᾓᾔᾕᾖᾗᾘᾙᾚᾛᾜᾝᾞᾟῂῃῄῆῇῊΉῌͰͱΉΗήηΘθϑϴἰἱἲἳἴἵἶἷἸἹἺἻἼἽἾἿὶίῐῑῒΐῖῗῘῙῚΊΊΐΙΪίιϊϳΚκϏϗϰΛλΜμΝνΞξὀὁὂὃὄὅὈὉὊὋὌὍὸόῸΌΌΟοόΠπϺϻῤῥῬΡρϱϼΣςσϲϹϽϾϿΤτὐὑὒὓὔὕὖὗὙὛὝὟὺύῠῡῢΰῦῧῨῩῪΎΎΥΫΰυϋύϒϓϔΦφϕΧχΨψὠὡὢὣὤὥὦὧὨὩὪὫὬὭὮὯὼώᾠᾡᾢᾣᾤᾥᾦᾧᾨᾩᾪᾫᾬᾭᾮᾯῲῳῴῶῷῺΏῼΏΩωώϖϚϛϜϝϞϟϘϙͲͳϠϡϷϸϢϣϤϥϦϧϨϩϪϫϬϭϮϯ','ααααααααααααααααααααααααααααααααααααααααααααααααααβββγγδδεεεεεεεεεεεεεεεεεεεεεεζζηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηθθθθιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιικκκκκλλμμννξξοοοοοοοοοοοοοοοοοοοοππϻϻρρρρρρρσσσσσσσσττυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυφφφχχψψωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωϛϛϝϝϟϟϙϙϠϠϡϡϸϸϣϣϥϥϧϧϩϩϫϫϭϭϯϯ')"/>
 <xsl:sort select="translate(tei:title, '’', ' ')" order="ascending"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listBibl>
    </xsl:template>

  

 

<!-- Classement alphabétique dans la table onomastique -->
    <xsl:template match="tei:listNym">
        <listNym>
            <xsl:for-each select="./tei:nym">
                <xsl:sort select="translate(tei:form,'ἀἁἂἃἄἅἆἇἈἉἊἋἌἍἎἏὰάᾀᾁᾂᾃᾄᾅᾆᾇᾈᾉᾊᾋᾌᾍᾎᾏᾰᾱᾲᾳᾴᾶᾷᾸᾹᾺΆᾼΆΑάαΒβϐΓγΔδἐἑἒἓἔἕἘἙἚἛἜἝὲέῈΈΈΕέεϵ϶ΖζἠἡἢἣἤἥἦἧἨἩἪἫἬἭἮἯὴήᾐᾑᾒᾓᾔᾕᾖᾗᾘᾙᾚᾛᾜᾝᾞᾟῂῃῄῆῇῊΉῌͰͱΉΗήηΘθϑϴἰἱἲἳἴἵἶἷἸἹἺἻἼἽἾἿὶίῐῑῒΐῖῗῘῙῚΊΊΐΙΪίιϊϳΚκϏϗϰΛλΜμΝνΞξὀὁὂὃὄὅὈὉὊὋὌὍὸόῸΌΌΟοόΠπϺϻῤῥῬΡρϱϼΣςσϲϹϽϾϿΤτὐὑὒὓὔὕὖὗὙὛὝὟὺύῠῡῢΰῦῧῨῩῪΎΎΥΫΰυϋύϒϓϔΦφϕΧχΨψὠὡὢὣὤὥὦὧὨὩὪὫὬὭὮὯὼώᾠᾡᾢᾣᾤᾥᾦᾧᾨᾩᾪᾫᾬᾭᾮᾯῲῳῴῶῷῺΏῼΏΩωώϖϚϛϜϝϞϟϘϙͲͳϠϡϷϸϢϣϤϥϦϧϨϩϪϫϬϭϮϯ','ααααααααααααααααααααααααααααααααααααααααααααααααααβββγγδδεεεεεεεεεεεεεεεεεεεεεεζζηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηηθθθθιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιιικκκκκλλμμννξξοοοοοοοοοοοοοοοοοοοοππϻϻρρρρρρρσσσσσσσσττυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυυφφφχχψψωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωωϛϛϝϝϟϟϙϙϠϠϡϡϸϸϣϣϥϥϧϧϩϩϫϫϭϭϯϯ')"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </listNym>
    </xsl:template>

    
</xsl:stylesheet>
